
import { AntdSkSwitch } from '../../sk-switch-antd/src/antd-sk-switch.js';

export class Antd4SkSwitch extends AntdSkSwitch {

    get prefix() {
        return 'antd4';
    }

}
